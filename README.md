<a href="https://f-droid.org/packages/org.nexttracks.android">
    <img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
    alt="Get it on F-Droid"
    height="80">
</a>

## Currently unmaintained
I don't really have the time to maintain NextTracks at the moment. 
You can still use it as it mostly works but I would suggest looking into the 
OSS flavour of OwnTracks (which now satisfies the initial goal of this project).

NextTracks for Android
=======
NextTracks is a fork of [OwnTracks](https://github.com/owntracks/android) which doesn't use Google Play Services.

It allows you to track the locations of your devices.

Supported Protocols: HTTP and MQTT

Features:

* You can use your own server.
* Map that shows your markers with your devices in it.
* Set waypoints to get a notification when a device leaves or joins a region (Geofencing).
* Friend List (each friend is a device, can be multiple per person) with their current address.
* Configurable to only publish manually or on significant movements.

See the [booklet](https://owntracks.org/booklet/features/android/) of OwnTracks for a list of known issues and restrictions.

# Screenshots

![screenshot](project/screenshot.png)
